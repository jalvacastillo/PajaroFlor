<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagoAlumno extends Model {

    protected $table = 'pagos_alumnos';
    protected $fillable = array(
        'alumno_id',
        'horas',
        'total'
    );

    public function alumno(){
        return $this->belongsTo('App\Models\Alumno', 'alumno_id');
    }

}


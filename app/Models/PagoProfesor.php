<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagoProfesor extends Model {

    protected $table = 'pagos_profesores';
    protected $fillable = array(
        'profesor_id',
        'alumno_id',
        'horas',
        'total'
    );

    public function alumno(){
        return $this->belongsTo('App\Models\Alumno', 'alumno_id');
    }

    public function profesor(){
        return $this->belongsTo('App\Models\Profesor', 'profesor_id');
    }

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clase extends Model {

    use SoftDeletes;
    protected $table = 'clases';
    protected $fillable = array(
        'inicio',
        'final',
        'horas',
        'estado'
    );

    protected $dates = ['inicio', 'final'];

    public function alumnos(){
        return $this->hasMany('App\Models\ClaseAlumno', 'clase_id');
    }

    public function profesores(){
        return $this->hasMany('App\Models\ClaseProfesor', 'clase_id');
    }


}


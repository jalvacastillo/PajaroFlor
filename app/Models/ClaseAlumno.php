<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaseAlumno extends Model {

    protected $table = 'clase_alumnos';
    protected $fillable = array(
        'clase_id',
        'alumno_id',
    );

    protected $appends = ['horas'];

    public function getHorasAttribute(){
        return $this->clase ? $this->clase->horas : null;
    }

    public function clase(){
        return $this->belongsTo('App\Models\Clase', 'clase_id');
    }

    public function alumno(){
        return $this->belongsTo('App\Models\Alumno', 'alumno_id');
    }

}


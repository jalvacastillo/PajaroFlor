<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaseProfesor extends Model {

    protected $table = 'clase_profesores';
    protected $fillable = array(
        'clase_id',
        'horas',
        'profesor_id',
    );

    public function clase(){
        return $this->belongsTo('App\Models\Clase', 'clase_id');
    }

    public function profesor(){
        return $this->belongsTo('App\Models\Profesor', 'profesor_id');
    }

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumno extends Model {

    use SoftDeletes;
    protected $table = 'alumnos';
    protected $fillable = array(
        'nombre',
        'nacionalidad',
        'sexo',
        'email',
        'telefono',
        'edad',
        'nivel',
    );

    public function clases(){
        return $this->hasMany('App\Models\ClaseAlumno', 'alumno_id');
    }

    public function pagos(){
        return $this->hasMany('App\Models\PagoAlumno', 'alumno_id');
    }

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LibroDiario extends Model {

    protected $table = 'libro_diario';
    protected $fillable = array(
        'fecha',
        'concepto',
        'tipo',
        'total'
    );

    protected $dates = ['fecha'];
    protected $appends = ['saldo'];

    public function getSaldoAttribute(){
        $ingresos = LibroDiario::where('id', '<=', $this->id)->where('tipo', 'Ingreso')->sum('total');
        $egresos = LibroDiario::where('id', '<=', $this->id)->where('tipo', 'Egreso')->sum('total');
        return $ingresos - $egresos;
    }

    public function alumno(){
        return $this->belongsTo('App\Models\Alumno', 'alumno_id');
    }

}


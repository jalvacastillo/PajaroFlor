<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profesor extends Model {

    use SoftDeletes;
    protected $table = 'profesores';
    protected $fillable = array(
        'nombre',
    );

    public function clases(){
        return $this->hasMany('App\Models\ClaseProfesor', 'profesor_id');
    }

    public function pagos(){
        return $this->hasMany('App\Models\PagoProfesor', 'profesor_id');
    }

}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PagoAlumno;
use App\Models\Alumno;

class PagosAlumnosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $pagos = PagoAlumno::orderBy('id', 'desc')->paginate(5);

        return view('dash.pagos_alumnos.index', compact('pagos'));
    }

    public function read($id = null)
    {   
        if ($id)
            $pago = PagoAlumno::with('alumno.clases.clase')->where('id', $id)->firstOrFail();
        else
            $pago = new PagoAlumno;

        $alumnos = Alumno::orderBy('id', 'desc')->get();

        return view('dash.pagos_alumnos.form', compact('pago', 'alumnos'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'alumno_id'       => 'required',
            'horas'       => 'required',
            'total'       => 'required',
        ]);

        if($request->id)
            $pago = PagoAlumno::findOrFail($request->id);
        else
            $pago = new PagoAlumno;

        $pago->fill($request->all());
        $pago->save();  

       return redirect('/admin/pagos-alumnos');
    }

    public function deletePago($id)
    {   
        $pago = PagoAlumno::find($id);
        $pago->delete();
        return back();
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clase;
use App\Models\ClaseAlumno;
use App\Models\ClaseProfesor;

use App\Models\Alumno;
use App\Models\Profesor;

class ClasesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $clases = Clase::orderBy('id', 'desc')->with('alumnos.alumno', 'profesores.profesor')->paginate(5);

        return view('dash.clases.index', compact('clases'));
    }

    public function read($id = null)
    {   
        if ($id)
            $clase = Clase::with('alumnos', 'profesores')->where('id', $id)->firstOrFail();
        else
            $clase = new Clase;

        $alumnos = Alumno::orderBy('id', 'desc')->get();
        $profesores = Profesor::orderBy('id', 'desc')->get();

        return view('dash.clases.form', compact('clase', 'alumnos', 'profesores'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'inicio'       => 'required',
            'final'       => 'required',
        ]);

        if($request->id)
            $clase = Clase::findOrFail($request->id);
        else
            $clase = new Clase;

        $clase->fill($request->all());
        $clase->save();  

       return redirect('/admin/clase/' . $clase->id);
        // return back();
    }

    public function deleteClase($id)
    {   
        $clase = Clase::find($id);
        $clase->delete();
        return back();
    }

    public function addAlumno(Request $request)
    {   
        $request->validate([
            'alumno_id'       => 'required',
            'clase_id'       => 'required',
        ]);

        $alumno = new ClaseAlumno;

        $alumno->fill($request->all());
        $alumno->save();  

        return back();
    }

    public function deleteAlumno($id)
    {   
        $alumno = ClaseAlumno::find($id);
        $alumno->delete();
        return back();
    }

    public function addProfesor(Request $request)
    {   

        $request->validate([
            'profesor_id'       => 'required',
            'horas'       => 'required:numeric',
            'clase_id'       => 'required',
        ]);

        $profesor = new ClaseProfesor;

        $profesor->fill($request->all());
        $profesor->save();  

       return back();
    }

    public function deleteProfesor($id)
    {   

        $profesor = ClaseProfesor::find($id);
        $profesor->delete();
        return back();
    }
    
}

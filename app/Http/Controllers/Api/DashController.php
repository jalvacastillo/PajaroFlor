<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use stdClass;

use App\Models\Empresa;
use App\Models\Habitaciones\Habitacion;
use App\Models\Tours\Tour;
use App\Models\Testimonio;
use App\Models\Galeria;

class DashController extends Controller
{

    public function index(Request $request) {

        $datos = new stdClass();

        $datos->total_habitaciones = Habitacion::count();
        $datos->total_testimonios = Testimonio::count();
        $datos->total_tours = Tour::count();
        $datos->total_galeria = Galeria::count();

        return Response()->json($datos, 200);
    }


}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Galeria;

class GaleriaController extends Controller
{
    

    public function index() {
       
        $galerias = Galeria::orderBy('id','desc')->get();

        return Response()->json($galerias, 200);

    }


    public function read($id) {

        $galeria = Galeria::findOrFail($id);
        return Response()->json($galeria, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'file'          => 'required_without:img|max:5048',
            'url'           => 'sometimes|max:255'
        ]);

        if($request->id)
            $galeria = Galeria::findOrFail($request->id);
        else
            $galeria = new Galeria;
        
        $galeria->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $galeria->url) {
                Storage::delete($galeria->url);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(960,960)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "galeria/{$hash}.jpg";
            $resize->save(public_path('imgs/'.$path), 50);
            $galeria->url = "/" . $path;
        }

        $galeria->save();

        return Response()->json($galeria, 200);

    }

    public function delete($id)
    {
        $galeria = Galeria::findOrFail($id);
        if ($galeria->url) {
            Storage::delete('imgs/'.$galeria->url);
        }
        $galeria->delete();

        return Response()->json($galeria, 201);

    }

}

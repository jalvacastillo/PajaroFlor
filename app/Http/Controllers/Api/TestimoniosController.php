<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Testimonio;

class TestimoniosController extends Controller
{
    

    public function index() {
       
        $testimonios = Testimonio::orderBy('id','desc')->get();

        return Response()->json($testimonios, 200);

    }


    public function read($id) {

        $testimonio = Testimonio::findOrFail($id);
        return Response()->json($testimonio, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre'        => 'required|max:255',
            'file'          => 'required_without:img|max:5048',
            'pais'          => 'required|max:255',
            'img'           => 'sometimes|max:255',
            'descripcion'   => 'required|max:1500',
        ]);

        if($request->id)
            $testimonio = Testimonio::findOrFail($request->id);
        else
            $testimonio = new Testimonio;
        
        $testimonio->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $testimonio->img) {
                Storage::delete($testimonio->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(350,350)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "testimonios/{$hash}.jpg";
            $resize->save(public_path('imgs/'.$path), 50);
            $testimonio->img = "/" . $path;
        }

        $testimonio->save();

        return Response()->json($testimonio, 200);

    }

    public function delete($id)
    {
        $testimonio = Testimonio::findOrFail($id);
        if ($testimonio->img) {
            Storage::delete('imgs/'.$testimonio->img);
        }
        $testimonio->delete();

        return Response()->json($testimonio, 201);

    }

}

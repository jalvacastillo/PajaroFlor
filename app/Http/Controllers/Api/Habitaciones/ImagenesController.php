<?php

namespace App\Http\Controllers\Api\Habitaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Habitaciones\Imagen;

class ImagenesController extends Controller
{
    

    public function index() {
       
        $imagenes = Imagen::orderBy('id','desc')->get();

        return Response()->json($imagenes, 200);

    }


    public function read($id) {

        $imagen = Imagen::findOrFail($id);
        return Response()->json($imagen, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'file'          => 'required_without:img|max:5048',
            'url'           => 'sometimes|max:255',
        ]);

        if($request->id)
            $imagen = Imagen::findOrFail($request->id);
        else
            $imagen = new Imagen;
        
        $imagen->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $imagen->url) {
                Storage::delete($imagen->url);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(960,960)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "habitaciones/{$hash}.jpg";
            $resize->save(public_path('imgs/'.$path), 50);
            $imagen->url = "/" . $path;
        }

        $imagen->save();

        return Response()->json($imagen, 200);

    }

    public function delete($id)
    {
        $imagen = Imagen::findOrFail($id);
        if ($imagen->url) {
            Storage::delete('imgs/'.$imagen->url);
        }
        $imagen->delete();

        return Response()->json($imagen, 201);

    }

}

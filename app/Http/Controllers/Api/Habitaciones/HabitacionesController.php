<?php

namespace App\Http\Controllers\Api\Habitaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Habitaciones\Habitacion;

class HabitacionesController extends Controller
{
    

    public function index() {
       
        $habitaciones = Habitacion::orderBy('id','desc')->get();

        return Response()->json($habitaciones, 200);

    }


    public function read($id) {

        $habitacion = Habitacion::where('id', $id)->with('imagenes')->firstOrFail();
        return Response()->json($habitacion, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre'        => 'required|max:255',
            'precio'        => 'required|numeric',
            'slug'          => 'required|unique:habitaciones,slug,'. $request->id,
            'tipo'          => 'required|max:255',
            'file'          => 'required_without:img|max:5048',
            'img'           => 'sometimes|max:255',
            'descripcion'   => 'required',
        ]);

        if($request->id)
            $habitacion = Habitacion::findOrFail($request->id);
        else
            $habitacion = new Habitacion;
        
        $habitacion->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $habitacion->img) {
                Storage::delete($habitacion->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(960,960)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "habitaciones/{$hash}.jpg";
            $resize->save(public_path('imgs/'.$path), 50);
            $habitacion->img = "/" . $path;
        }

        $habitacion->save();

        return Response()->json($habitacion, 200);

    }

    public function delete($id)
    {
        $habitacion = Habitacion::where('id', $id)->with('imagenes')->firstOrFail();
        if ($habitacion->img) {
            Storage::delete('imgs/'.$habitacion->img);
        }
        foreach ($habitacion->imagenes as $imagen) {
            if ($imagen->url) {
                Storage::delete('imgs/'.$imagen->url);
            }
            $imagen->delete();
        }
        $habitacion->delete();

        return Response()->json($habitacion, 201);

    }

}

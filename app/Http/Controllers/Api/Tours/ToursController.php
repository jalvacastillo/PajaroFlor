<?php

namespace App\Http\Controllers\Api\Tours;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Tours\Tour;

class ToursController extends Controller
{
    

    public function index() {
       
        $tours = Tour::orderBy('id','desc')->get();

        return Response()->json($tours, 200);

    }


    public function read($id) {

        $tour = Tour::findOrFail($id);
        return Response()->json($tour, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre'        => 'required|max:255',
            'precio'        => 'required|numeric',
            'duracion'      => 'required|max:255',
            'file'          => 'required_without:img|max:5048',
            'url'           => 'sometimes|max:255',
            'descripcion'   => 'required',
        ]);

        if($request->id)
            $tour = Tour::findOrFail($request->id);
        else
            $tour = new Tour;
        
        $tour->fill($request->all());
        if ($request->hasFile('file')) {
            if ($request->id && $tour->img) {
                Storage::delete($tour->img);
            }
            $path   = $request->file('file');
            $resize = Image::make($path)->resize(960,960)->encode('jpg', 75);
            $hash = md5($resize->__toString());
            $path = "tours/{$hash}.jpg";
            $resize->save(public_path('imgs/'.$path), 50);
            $tour->img = "/" . $path;
        }
        $tour->save();

        return Response()->json($tour, 200);

    }

    public function delete($id)
    {
        $tour = Tour::findOrFail($id);
        if ($tour->img) {
            Storage::delete('imgs/'.$tour->img);
        }
        $tour->delete();

        return Response()->json($tour, 201);

    }

}

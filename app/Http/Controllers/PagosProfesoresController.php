<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PagoProfesor;
use App\Models\Alumno;
use App\Models\Profesor;

class PagosProfesoresController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $pagos = PagoProfesor::orderBy('id', 'desc')->paginate(5);

        return view('dash.pagos_profesores.index', compact('pagos'));
    }

    public function read($id = null)
    {   
        if ($id)
            $pago = PagoProfesor::with('alumno.clases.clase')->where('id', $id)->firstOrFail();
        else
            $pago = new PagoProfesor;

        $alumnos = Alumno::orderBy('id', 'desc')->get();
        $profesores = Profesor::orderBy('id', 'desc')->get();

        return view('dash.pagos_profesores.form', compact('pago', 'alumnos', 'profesores'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'profesor_id'       => 'required',
            'alumno_id'       => 'required',
            'horas'       => 'required',
            'total'       => 'required',
        ]);

        if($request->id)
            $pago = PagoProfesor::findOrFail($request->id);
        else
            $pago = new PagoProfesor;

        $pago->fill($request->all());
        $pago->save();  

       return redirect('/admin/pagos-profesores');
    }

    public function deletePago($id)
    {   
        $pago = PagoProfesor::find($id);
        $pago->delete();
        return back();
    }
    
}

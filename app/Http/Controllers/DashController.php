<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Alumno;
use App\Models\Profesor;
use App\Models\Clase;
use App\Models\PagoAlumno;
use App\Models\PagoProfesor;

class DashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $alumnos    = Alumno::count();
        $profesores = Profesor::count();
        $clases     = Clase::count();
        $pagosAlumnos      = PagoAlumno::whereMonth('created_at', date('m'))->sum('total');
        $pagosProfesores      = PagoProfesor::whereMonth('created_at', date('m'))->sum('total');

        return view('dash.index', compact('alumnos', 'profesores', 'clases', 'pagosAlumnos', 'pagosProfesores'));
    }
    
}

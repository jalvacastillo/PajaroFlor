<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\PagoAlumno;
use App\Models\ClaseAlumno;

class AlumnosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $alumnos = Alumno::orderBy('id', 'desc')->paginate(5);

        return view('dash.alumnos.index', compact('alumnos'));
    }

    public function read($id = null)
    {   
        if ($id)
            $alumno = Alumno::with('clases.clase')->where('id', $id)->firstOrFail();
        else
            $alumno = new Alumno;

        return view('dash.alumnos.form', compact('alumno'));
    }

    public function clases($id)
    {   
        if ($id){
            $alumno = Alumno::where('id', $id)->firstOrFail();
            $clases = ClaseAlumno::where('alumno_id', $alumno->id)->paginate(7);
        }
        else{
            return redirect::back();
        }

        return view('dash.alumnos.clases', compact('alumno', 'clases'));
    }

    public function pagos($id)
    {   
        if ($id){
            $alumno = Alumno::where('id', $id)->firstOrFail();
            $pagos = PagoAlumno::where('alumno_id', $alumno->id)->paginate(7);
        }
        else{
            return redirect::back();
        }

        return view('dash.alumnos.pagos', compact('alumno', 'pagos'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'nombre'       => 'required',
        ]);

        if($request->id)
            $alumno = Alumno::findOrFail($request->id);
        else
            $alumno = new Alumno;

        $alumno->fill($request->all());
        $alumno->save();  

       return redirect('/admin/alumnos');
    }

    public function deleteAlumno($id)
    {   
        $alumno = Alumno::find($id);
        $alumno->delete();
        return back();
    }
    
}

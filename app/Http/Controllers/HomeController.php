<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use Mail;
use Request;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function correo(){


        try {

            $data = Request::except('_token', 'g-recaptcha-response');
            
            if ( $data['nombre'] != '' && $data['mensaje'] != '' && $data['correo'] != '') {
                
                Mail::send('emails.cotizacion', ['data' => $data], function ($m) use ($data) {
                    $m->from('hello@pajaroflor.com', 'Página web - Pajaro Flor')
                    ->to('hello@pajaroflor.com', 'Página web - Pajaro Flor')
                    // ->to('alvarado.websis@gmail.com', 'Página web - Mensaje')
                    ->cc('pajaroflorsv@gmail.com')
                    ->cc('alvarado.websis@gmail.com')
                    ->replyTo($data['correo'])
                    ->subject('pajaroflor.com');
                });
                return redirect('/#footer')->with('message', 'Great, we have received your message!');
            }else{
                return redirect('/#footer')->with('message', 'Woops, complete all the fields!');
            }


        } catch (Exception $e) {
            
            return redirect('/#footer')->with('message', 'There was an error sending your message, please try again!');
            
        }

    }

    public function registro(){


        try {
            $data = Request::except('_token', 'g-recaptcha-response');
        
            // $data['inicio'] = Carbon::parse($data['inicio'])->format('d-m-Y h:i:s A');
            // $data['fin'] = Carbon::parse($data['fin'])->format('d-m-Y h:i:s A');

            if ( strtolower($data['nacionalidad']) != 'russian' && (strtolower($data['nombre']) != strtolower($data['apellido'])) && (strtolower($data['nombre']) != strtolower($data['nacionalidad'])) && (strtolower($data['interes']) != strtolower($data['nacionalidad']))) {
                
                Mail::send('emails.cotizacion', ['data' => $data], function ($m) use ($data) {
                    $m->from('hello@pajaroflor.com', 'Página web - Pajaro Flor')
                    ->to('hello@pajaroflor.com', 'Página web - Registro')
                    ->cc('pajaroflorsv@gmail.com')
                    ->cc('alvarado.websis@gmail.com')
                    ->replyTo($data['correo'])
                    ->subject('pajaroflor.com');
                });

                return redirect('/#footer')->with('message', 'Great, we have received your message!');
            }else{
                // return redirect('/#footer')->with('message', 'Woops, sorry a span has been detected!');
                return redirect('/#footer')->with('message', 'Great, we have received your message!');
            }

        } catch (Exception $e) {
            
            return redirect('/#footer')->with('message', 'There was an error sending your message, please try again!');
            
        }

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LibroDiario;

class LibroDiarioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $registros = LibroDiario::orderBy('fecha', 'desc')->orderBy('id', 'desc')->paginate(5);

        return view('dash.libro.index', compact('registros'));
    }

    public function read($id = null)
    {   
        if ($id){
            $registro = LibroDiario::where('id', $id)->firstOrFail();
        }
        else{
            $registro = new LibroDiario;
            $registro->fecha = date('Y-m-d');
            $registro->tipo = 'Ingreso';
        }

        return view('dash.libro.form', compact('registro'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'fecha'       => 'required',
            'concepto'       => 'required',
            'tipo'       => 'required',
            'total'       => 'required',
        ]);

        if($request->id)
            $registro = LibroDiario::findOrFail($request->id);
        else
            $registro = new LibroDiario;

        $registro->fill($request->all());
        $registro->save();  

       return redirect('/admin/libro-diario');
    }

    public function deleteRegistro($id)
    {   
        $registro = LibroDiario::find($id);
        $registro->delete();
        return back();
    }
    
}

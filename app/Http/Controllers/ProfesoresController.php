<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profesor;
use App\Models\PagoProfesor;
use App\Models\ClaseProfesor;

class ProfesoresController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $profesores = Profesor::orderBy('id', 'desc')->paginate(5);

        return view('dash.profesores.index', compact('profesores'));
    }

    public function read($id = null)
    {   
        if ($id)
            $profesor = Profesor::with('clases.clase.alumnos')->where('id', $id)->firstOrFail();
        else
            $profesor = new Profesor;

        return view('dash.profesores.form', compact('profesor'));
    }

    public function clases($id)
    {   
        if ($id){
            $profesor = Profesor::where('id', $id)->firstOrFail();
            $clases = ClaseProfesor::where('profesor_id', $profesor->id)->paginate(7);
        }
        else{
            return redirect::back();
        }

        return view('dash.profesores.clases', compact('profesor', 'clases'));
    }

    public function pagos($id)
    {   
        if ($id){
            $profesor = Profesor::where('id', $id)->firstOrFail();
            $pagos = PagoProfesor::where('profesor_id', $profesor->id)->paginate(7);
        }
        else{
            return redirect::back();
        }

        return view('dash.profesores.pagos', compact('profesor', 'pagos'));
    }

    public function store(Request $request)
    {   

        $request->validate([
            'nombre'       => 'required',
        ]);

        if($request->id)
            $profesor = Profesor::findOrFail($request->id);
        else
            $profesor = new Profesor;

        $profesor->fill($request->all());
        $profesor->save();  

       return redirect('/admin/profesores');
    }

    public function deleteProfesor($id)
    {   
        $profesor = Profesor::find($id);
        $profesor->delete();
        return back();
    }
    
}

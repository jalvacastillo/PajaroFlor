<!DOCTYPE html>
<html lang="en" ng-app="SpanishApp">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pajaro Flor - Spanish School</title>
  <meta name="description" content="Pajaro Flor Community Language Center Spanish School Suchitoto El salvador. Our teachers were trained by theformer Spanish language Director for Peace Corps El Salvador to teachusing internationally recognized methods.">
  <meta name="keywords" content="suchitoto,spanish,school,learn spanish,tours,travel,el salvador">
  <meta name="Reply-to" content="hello@pajaroflor.com">
  <meta name="robots" content="index, follow" >
  <meta name="author" content="Jesus Alvarado">
  <link rel="shortcut icon" type="image/ico" href="{{ asset('/favicon.ico') }}"/>

  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/imagehover.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script> function recaptchaC_callback(){$('#recaptchaC').removeAttr('disabled'); } </script>
  <script> function recaptchaR_callback(){$('#recaptchaR').removeAttr('disabled'); } </script>

  <script src="{{ asset('js/angular.min.js') }}"></script>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-79TB99ZNER"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-79TB99ZNER');
  </script>
  
</head>


<body>

  @yield('content')
  
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>

  <script>

    angular.module('SpanishApp', [])
    .controller('PresupuestoCtr', ['$scope', function($scope) {

        $scope.dias = 1;
        $scope.horas = '4';
        $scope.total = 0;
        $scope.tipo = '';

        $scope.extra = 'no';
        $scope.transporte = 'no';

        $scope.getPresupuesto = function() {

          var totalHoras = $scope.dias * $scope.horas;

            if($scope.tipo == 'Presencial'){
                $scope.precio = 12;       //1-39
                if(totalHoras >= 40){     //40-59
                    $scope.precio = 11.50;
                }
                if(totalHoras >= 60){     //60-75
                    $scope.precio = 11;
                }
                if(totalHoras >= 80){      //76...
                    $scope.precio = 10;
                }

                $scope.total = totalHoras * $scope.precio;
            }
            if($scope.tipo == 'Online'){
                if($scope.onlinehoras == 5){
                    $scope.total = 65;
                }if($scope.onlinehoras == 10){
                    $scope.total = 120;
                }if($scope.onlinehoras == 20){
                    $scope.total = 220;
                }if($scope.onlinehoras == 50){
                    $scope.total = 500;
                }
            }

            if($scope.extra == 1){
              $scope.total += (10.50 * $scope.dias);
            }
            if($scope.extra == 2){
              $scope.total += (18 * $scope.dias);
            }

            if($scope.transporte == 1){
              $scope.total += 60;
            }
            if($scope.transporte == 2){
              $scope.total += 120;
            }

        };

    }]);

  </script>

</body>

</html>

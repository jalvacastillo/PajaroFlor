@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pull-left">Alumnos <small class="badge badge-info">{{ $alumnos->total() }}</small></h4>
                            <a href="{{ route('alumno') }}" class="btn btn-primary pull-right">Nuevo</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Nacionalidad</th>
                                <th>Sexo</th>
                                <th class="text-center">Edad</th>
                                <th class="text-center">Nivel</th>
                                <th class="text-center">Clases</th>
                                <th class="text-center">Horas</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alumnos as $alumno)
                            <tr>
                                <td>{{ $alumno->nombre }}</td>
                                <td>{{ $alumno->nacionalidad }}</td>
                                <td>{{ $alumno->sexo }}</td>
                                <td class="text-center">{{ $alumno->edad }}</td>
                                <td class="text-center">{{ $alumno->nivel }}</td>
                                <td class="text-center">{{ $alumno->clases->count() }}</td>
                                <td class="text-center">{{ $alumno->clases->sum('horas') }}</td>
                                <td class="text-center">
                                    <a href="{{ route('alumno', $alumno->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Editar">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{ route('alumnoPagos', $alumno->id) }}" class="btn btn-sm btn-default"  data-toggle="tooltip" title="Pagos">
                                        <i class="fa fa-money-bill"></i>
                                    </a>
                                    <a href="{{ route('alumnoClases', $alumno->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Clases">
                                        <i class="fa fa-chalkboard-teacher"></i>
                                    </a>
                                    <a href="{{ route('deleteAlumno', $alumno->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                        <i class="fa fa-trash">
                                    </i>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $alumnos->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

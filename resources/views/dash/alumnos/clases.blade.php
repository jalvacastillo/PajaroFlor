@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h4 class="col-xs-12 text-center">Alumno: <b>{{ $alumno->nombre }}</b></h4>
    </div>

    @if ($clases->count() > 0)
    <div class="row">
        <div class="col-md-12">
        <form method="POST">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Clases  <small class="badge badge-info">{{ $alumno->clases->count() }}</small></h4>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Inicio</th>
                                <th>Finalizo</th>
                                <th  class="text-center">Horas</th>
                                <th  class="text-center">Estado</th>
                                <th  class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alumno->clases as $clase)
                            <tr>
                                <td>{{ $clase->clase->inicio->format('d/m/Y') }}</td>
                                <td>{{ $clase->clase->final->format('d/m/Y') }}</td>
                                <td class="text-center"> {{ $clase->clase->horas }} </td>
                                <td class="text-center"> {{ $clase->clase->estado }} </td>
                                <td class="text-center">
                                    <a href="{{ route('clase', $clase->clase->id) }}" class="btn btn-default">
                                    <i class="fa fa-arrow-right"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $clases->links() }}
                </div>
            </div>
        </div>
        </form>
    </div>
    @endif
</div>
@endsection

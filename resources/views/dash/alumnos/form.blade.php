@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form method="POST">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Alumno</h4>
                </div>
                {!! csrf_field() !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="nombre" class="form-control" value="{{ $alumno->nombre }}" autofocus required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nacionalidad">Nacionalidad:</label>
                                <input type="text" name="nacionalidad" class="form-control" value="{{ $alumno->nacionalidad }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="sexo">Sexo:</label>
                                <select name="sexo" class="form-control">
                                    <option value="Hombre" {{ ($alumno->sexo == 'Hombre') ? 'selected' : '' }}>Hombre</option>
                                    <option value="Mujer" {{ ($alumno->sexo == 'Mujer') ? 'selected' : '' }}>Mujer</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email">Correo:</label>
                                <input type="email" name="email" class="form-control" value="{{ $alumno->email }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="telefono">Teléfono:</label>
                                <input type="tel" name="telefono" class="form-control" value="{{ $alumno->telefono }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="edad">Edad:</label>
                                <input type="number" name="edad" class="form-control" value="{{ $alumno->edad }}">
                            </div>
                        </div>                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nivel">Nivel de español:</label>
                                <select name="nivel" class="form-control">
                                    <option value="Básico" {{ ($alumno->sexo == 'Básico') ? 'selected' : '' }}>Básico</option>
                                    <option value="Intermedio" {{ ($alumno->sexo == 'Intermedio') ? 'selected' : '' }}>Intermedio</option>
                                    <option value="Avanzado" {{ ($alumno->sexo == 'Avanzado') ? 'selected' : '' }}>Avanzado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

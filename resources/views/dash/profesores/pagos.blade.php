@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h4 class="col-xs-12 text-center">Profesor: <b>{{ $profesor->nombre }}</b></h4>
    </div>

    @if ($pagos->count() > 0)
    <div class="row">
        <div class="col-md-12">
        <form method="POST">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Pagos  <small class="badge badge-info">$ {{ $profesor->pagos->sum('total') }}</small></h4>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Alumno</th>
                                <th  class="text-center">Horas</th>
                                <th  class="text-center">Total</th>
                                <th  class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($profesor->pagos as $pago)
                            <tr>
                                <td>{{ $pago->created_at->format('d/m/Y') }}</td>
                                <td> {{ $pago->alumno->nombre }} </td>
                                <td class="text-center"> {{ $pago->horas }} </td>
                                <td class="text-center"> $ {{ $pago->total }} </td>
                                <td class="text-center">
                                    <a href="{{ route('pagoProfesor', $pago->id) }}" class="btn btn-default">
                                    <i class="fa fa-arrow-right"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3" class="text-right"> <b>Total:</b> </td>
                                <td class="text-center"> <b>$ {{ number_format($pagos->sum('total'),2) }}</b> </td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $pagos->links() }}
                </div>
            </div>
        </div>
        </form>
    </div>
    @endif
</div>
@endsection

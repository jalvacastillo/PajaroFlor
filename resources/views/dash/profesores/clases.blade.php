@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h4 class="col-xs-12 text-center">Profesor: <b>{{ $profesor->nombre }}</b></h4>
    </div>

    @if ($clases->count() > 0)
    <div class="row">
        <div class="col-md-12">
        <form method="POST">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Clases</h4>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Alumno</th>
                                <th  class="text-center">Horas</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($profesor->clases as $clase)
                            <tr>
                                <td>
                                    @foreach ($clase->clase->alumnos as $alumno)
                                        {{ $alumno->alumno->nombre }} <br>
                                    @endforeach
                                </td>
                                <td class="text-center"> {{ $clase->horas }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-right"> <b>Total:</b> </td>
                                <td class="text-center"> <b>{{ $profesor->clases()->sum('horas') }}</b> </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $clases->links() }}
                </div>
            </div>
        </div>
        </form>
    </div>
    @endif
</div>
@endsection

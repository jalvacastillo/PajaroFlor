@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pull-left">Profesores <small class="badge badge-info">{{ $profesores->total() }}</small></h4>
                            <a href="{{ route('profesor') }}" class="btn btn-primary pull-right">Nuevo</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th class="text-center">Clases</th>
                                <th class="text-center">Horas</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($profesores as $profesor)
                            <tr>
                                <td>{{ $profesor->nombre }}</td>
                                <td class="text-center">{{ $profesor->clases()->count() }}</td>
                                <td class="text-center">{{ $profesor->clases()->sum('horas') }}</td>
                                <td class="text-center">

                                    <a href="{{ route('profesor', $profesor->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Editar">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{ route('profesorPagos', $profesor->id) }}" class="btn btn-sm btn-default"  data-toggle="tooltip" title="Pagos">
                                        <i class="fa fa-money-bill"></i>
                                    </a>
                                    <a href="{{ route('profesorClases', $profesor->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Clases">
                                        <i class="fa fa-chalkboard-teacher"></i>
                                    </a>
                                    <a href="{{ route('deleteProfesor', $profesor->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                        <i class="fa fa-trash">
                                    </i>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $profesores->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

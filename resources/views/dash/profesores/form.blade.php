@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <form method="POST">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Profesor</h4>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="nombre" class="form-control" value="{{ $profesor->nombre }}" autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>


</div>
@endsection

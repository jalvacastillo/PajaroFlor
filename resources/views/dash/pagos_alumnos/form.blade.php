@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form method="POST">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Pago Alunmno</h4>
                </div>
                {!! csrf_field() !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="alumno_id">Alumno:</label>
                                <select name="alumno_id" class="form-control">
                                    <option disabled selected>Seleccione uno</option>
                                    @foreach ($alumnos as $alumno)
                                        <option value="{{ $alumno->id }}" {{ $alumno->id == $pago->alumno_id ? 'selected' : '' }}>{{ $alumno->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="horas">Horas:</label>
                                <input type="number" name="horas" step="any" class="form-control" value="{{ $pago->horas }}">
                            </div>
                        </div>      
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="total">Total:</label>
                                <input type="number" name="total" step="any" class="form-control" value="{{ $pago->total }}">
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

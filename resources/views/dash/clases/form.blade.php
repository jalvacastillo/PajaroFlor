@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row">

        <div class="col-md-12">
        <form method="POST">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Clase</h4>
                </div>

                {!! csrf_field() !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inicio">Inicio:</label>
                                <input type="date" name="inicio" class="form-control" value="{{ $clase->inicio ? $clase->inicio->format('Y-m-d') : date('Y-m-d') }}" autofocus>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="final">Final:</label>
                                <input type="date" name="final" class="form-control" value="{{ $clase->final ? $clase->final->format('Y-m-d') : date('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="horas">Horas:</label>
                                <input type="number" name="horas" class="form-control" value="{{ $clase->horas }}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="estado">Estado:</label>
                                <select name="estado" class="form-control">
                                    <option value="Creada" {{ ($clase->estado == 'Creada') ? 'selected' : '' }}>Creada</option>
                                    <option value="En Proceso" {{ ($clase->estado == 'En Proceso') ? 'selected' : '' }}>En Proceso</option>
                                    <option value="Finalizada" {{ ($clase->estado == 'Finalizada') ? 'selected' : '' }}>Finalizada</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    
    </div>
    
    @if ($clase->id)
    <div class="row">

        <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form method="POST" action="{{ route('addAlumnoToClase') }}" class="form-inline">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="alumno_id">Alumno:</label>
                        <select name="alumno_id" class="form-control">
                            @foreach ($alumnos as $alumno)
                                <option value="{{ $alumno->id }}">{{ $alumno->nombre }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="clase_id" value="{{ $clase->id }}">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Pais</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clase->alumnos as $alumno)
                        <tr>
                            <td>{{ $alumno->alumno->nombre }}</td>
                            <td>{{ $alumno->alumno->nacionalidad }}</td>
                            <td class="text-center">
                                <a href="{{ route('deleteAlumnoToClase', $alumno->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>

        <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form method="POST" action="{{ route('addProfesorToClase') }}" class="form-inline">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="profesor_id">Profesor:</label>
                        <select name="profesor_id" class="form-control">
                            @foreach ($profesores as $profesor)
                                <option value="{{ $profesor->id }}">{{ $profesor->nombre }}</option>
                            @endforeach
                        </select>
                        <label for="horas">Horas:</label>
                        <input type="number" name="horas" class="form-control" style="width: 100px;" required>
                        <input type="hidden" name="clase_id" value="{{ $clase->id }}">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Horas</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clase->profesores as $profesor)
                        <tr>
                            <td>{{ $profesor->profesor()->first()->nombre }}</td>
                            <td>{{ $profesor->horas }}</td>
                            <td class="text-center">
                                <a href="{{ route('deleteProfesorToClase', $profesor->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>

    </div>
    @endif
    
</div>
@endsection

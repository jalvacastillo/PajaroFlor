@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pull-left">Clases <small class="badge badge-info">{{ $clases->total() }}</small></h4>
                            <a href="{{ route('clase') }}" class="btn btn-primary pull-right">Nuevo</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Alumno</th>
                                <th>Profesor</th>
                                <th>Inicio</th>
                                <th>Final</th>
                                <th class="text-center">Horas</th>
                                <th>Estado</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clases as $clase)
                            <tr>
                                <td>
                                    @foreach ($clase->alumnos as $alumno)
                                        {{ $alumno->alumno->nombre }} <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($clase->profesores as $profesor)
                                        {{ $profesor->profesor->nombre }} - <small><b>{{ $profesor->horas }} horas</b></small><br>
                                    @endforeach
                                </td>
                                <td>{{ $clase->inicio->format('d/m/Y') }}</td>
                                <td>{{ $clase->final->format('d/m/Y') }}</td>
                                <td class="text-center">{{ $clase->horas }}</td>
                                <td>{{ $clase->estado }}</td>
                                <td class="text-center">
                                    <a href="{{ route('clase', $clase->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Editar">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{ route('deleteClase', $clase->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                        <i class="fa fa-trash">
                                    </i>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $clases->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

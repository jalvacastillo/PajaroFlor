@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Indicadores</div>

                <div class="panel-body">
                    
                    <div class="col-sm-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading"> <h2>{{ $clases }}</h2> </div>
                            <div class="panel-body"> Clases </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-warning ">
                            <div class="panel-heading"> <h2>{{ $alumnos }}</h2> </div>
                            <div class="panel-body"> Alumnos </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-danger">
                            <div class="panel-heading"> <h2>{{ $profesores }}</h2> </div>
                            <div class="panel-body"> Profesores </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-success">
                            <div class="panel-heading"> <h2>$ {{ number_format($pagosAlumnos, 2) }}</h2> </div>
                            <div class="panel-body"> Pago de Alumnos</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-success">
                            <div class="panel-heading"> <h2>$ {{ number_format($pagosProfesores, 2) }}</h2> </div>
                            <div class="panel-body"> Pago a Profesores</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pull-left">Libro Diario <small class="badge badge-info">{{ $registros->total() }}</small></h4>
                            <a href="{{ route('libroRegistro') }}" class="btn btn-primary pull-right">Nuevo</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Concepto</th>
                                <th class="text-center">Ingreso</th>
                                <th class="text-center">Egreso</th>
                                <th class="text-center">Saldo</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($registros as $registro)
                            <tr>
                                <td>{{ $registro->fecha->format('d/m/Y') }}</td>
                                <td>{{ $registro->concepto }}</td>
                                <td class="text-center">{{ $registro->tipo == 'Ingreso' ? '$'.number_format($registro->total, 2) : ''}}</td>
                                <td class="text-center">{{ $registro->tipo == 'Egreso' ? '$'.number_format($registro->total, 2) : ''}}</td>
                                <td class="text-center">$ {{ number_format($registro->saldo, 2) }}</td>
                                <td class="text-center">
                                    <a href="{{ route('libroRegistro', $registro->id) }}" class="btn btn-sm btn-default"  data-toggle="tooltip" title="Editar">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{ route('deleteRegistro', $registro->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                        <i class="fa fa-trash">
                                    </i>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $registros->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

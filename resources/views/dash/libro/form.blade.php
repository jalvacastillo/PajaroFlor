@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form method="POST">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Libro Diario</h4>
                </div>
                {!! csrf_field() !!}
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fecha">Fecha:</label>
                                <input type="date" name="fecha" class="form-control" value="{{ $registro->fecha->format('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="concepto">Concepto:</label>
                                <input type="text" name="concepto" class="form-control" value="{{ $registro->concepto }}">
                            </div>
                        </div>     
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tipo">Tipo:</label> <br>
                                <div style="text-align: center;">
                                    <input type="radio" name="tipo" value="Ingreso" {{ $registro->tipo == 'Ingreso' ? 'checked' : '' }}> Ingreso
                                    <span style="margin-left: 100px;"></span>
                                    <input type="radio" name="tipo" value="Egreso" {{ $registro->tipo == 'Egreso' ? 'checked' : '' }}> Egreso
                                </div>
                            </div>
                        </div>      
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="total">Total:</label>
                                <input type="number" step="any" name="total" class="form-control" value="{{ $registro->total }}">
                            </div>
                        </div>      
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

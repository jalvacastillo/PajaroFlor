@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="pull-left">Pagos de Profesores <small class="badge badge-info">{{ $pagos->total() }}</small></h4>
                            <a href="{{ route('pagoProfesor') }}" class="btn btn-primary pull-right">Nuevo</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Profesor</th>
                                <th>Alumno</th>
                                <th class="text-center">Horas</th>
                                <th class="text-center">Total</th>
                                <th class="text-center"><i class="fa fa-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pagos as $pago)
                            <tr>
                                <td>{{ $pago->created_at->format('d/m/Y') }}</td>
                                <td>{{ $pago->profesor->nombre }}</td>
                                <td>{{ $pago->alumno->nombre }}</td>
                                <td class="text-center">{{ $pago->horas }}</td>
                                <td class="text-center">$ {{ $pago->total }}</td>
                                <td class="text-center">
                                    <a href="{{ route('pagoProfesor', $pago->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Editar">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{ route('deletePagoProfesor', $pago->id) }}" onclick="return confirm('esta seguro?')" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar">
                                        <i class="fa fa-trash">
                                    </i>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer text-center no-padding">
                    {{ $pagos->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<footer id="footer" class="footer">
    <div class="container">

      <h3 style="color: white; text-align: center;">Contact Us</h3>

      <div class="row">
        <div class="header-section text-center">
          <p>Feel free to send us a message.</p>
          <hr class="bottom-line">
        </div>
        <div class="row" style="margin: 15px 15px 50px 15px;">
          <div class="col-xs-12 col-md-4" style="border-left: 3px solid #5fcf80;">
            <b>Postal address</b> <br>
            4a. CallePoniente #22, Barrio <br>
            San Jose,Suchitoto El Salvador.
          </div>
          <div class="col-xs-12 col-md-4" style="border-left: 3px solid #5fcf80;">
            <b>Office Hours</b><br>
            8:00am - 6:00pm Monday-Friday <br>
            8:00am - 12:00pm Saturday
          </div>
          <div class="col-xs-12 col-md-4" style="border-left: 3px solid #5fcf80;">
            <b>Call Us</b> <br>
            hello@pajaroflor.com <br>
            Phones: (503) 72307812 or 23272366
          </div>
        </div>
      
      @if(session()->has('message'))
      <div class="row text-center">
        <div class="col-xs-12">
          <h2 style="color: white; margin-bottom: 0px;"> {{ session()->get('message') }} </h2>
        </div>
      </div>
      @endif

      <!-- End newsletter-form -->
      <ul class="social-links text-center">
        {{-- <li><a href="#link"><i class="fa fa-twitter fa-fw"></i></a></li> --}}
        <li><a href="https://www.facebook.com/SpanishSchoolPajaroFlor/" target="_blank"><i class="fab fa-facebook-square fa-2x" style="color: #3b5998;"></i></a></li>
        <li><a href="https://m.me/SpanishSchoolPajaroFlor" target="_blank"><i class="fab fa-facebook-messenger fa-2x" style="color: #0084ff;"></i></a></li>
        {{-- <li><a href="#link"><i class="fa fa-facebook-messenger fa-fw"></i></a></li> --}}
        {{-- <li><a href="#link"><i class="fa fa-youtube fa-fw"></i></a></li> --}}
        <li><a href="https://api.whatsapp.com/send?phone=50371077371" target="_blank"><i class="fab fa-whatsapp-square fa-2x" style="color: #25D366;"></i></a></li>
        <li><a data-toggle="modal" href='#modal-id'><i class="fas fa-envelope-square fa-2x" style="color: #e8590c;"></i></a></li>
        <li><a href="tel:+50323272366" target="_blank"><i class="fas fa-phone-square fa-2x" style="color: #adb5bd;"></i></a></li>
      </ul>
      <div class="credits text-center">
        Con el apoyo de <a href="http://cri.catolica.edu.sv/cdmype" target="_blank">CDMYPE Ilobasco.</a><br>
        ©{{date('Y')}} All rights reserved
      </div>
    </div>
  </footer>

  <div class="modal fade" id="modal-id">
    <div class="modal-dialog" style="margin-top: 30px;">
      <div class="modal-content">
        <form method="POST" action="{{ route('correo') }}">
          {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class=" text-center modal-title">Write us!</h4>
          </div>
          <div class="modal-body">
            <label for="nombre">Your name:</label>
            <input type="text" name="nombre" class="form-control" required placeholder="Name and Last name" autofocus>
            <label for="nombre">Your email:</label>
            <input type="email" name="correo" class="form-control" required placeholder="your@email.com">
            <label for="nombre">Your message:</label>
            <textarea name="mensaje" cols="30" class="form-control" required rows="6" placeholder="Tell us about you..."></textarea>
            <div class="form-group">
              <div class="g-recaptcha" data-sitekey="6LeSqJgUAAAAAKbvyvBKFpYpydDFQKR3OkFZg3Wr" data-callback="recaptchaC_callback"></div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="recaptchaC"  disabled>Send</button>
          </div>
        </form>
      </div>
    </div>
  </div>

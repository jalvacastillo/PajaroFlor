<section id="feature" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2>Background</h2>
          <p class="col-xs-12">Pájaro Flor Community Language Center is an initiative of Suchitotenses for the Education and Development of Suchitoto</p>
          <hr class="bottom-line">
        </div>
        <div class="feature-info">
          <div class="fea">
            <div class="col-md-4 text-center">
                <br>
                <img src="img/logo.jpg" alt="Logo Pajaro Flor" width="200px">
                <br>
            </div>
            <div class="col-md-8 tabs" role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#home" aria-controls="home" role="tab" data-toggle="tab">General information</a>
                </li>
                <li role="presentation">
                  <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">History</a>
                </li>
              </ul>
            
              <!-- Tab panes -->
              <div class="tab-content" style="padding: 15px;">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <p class="text-justify">
                    The Pajaro Flor Spanish School is an initiative of the people of Suchitoto which began offering
                    Spanish courses in November 2006. We will have 12 years of operating and one of our main
                    objectives, apart from offering excellent quality Spanish classes, is to provide opportunities of
                    employment to the local youth. Our teachers are young from Suchitoto who are well trained and
                    have a lot of experience teaching English. <br>
                    Come and join our efforts, in which you’ll learn “a language is a culture” While taking courses
                    with us, you will be contributing to the social economic and cultural development of Suchitoto.
                  </p>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab">
                  <p class="text-justify">
                    The idea of ​​creating the Spanish School was born when a group of suchitotenses were
                    part of a local liaison committee with Suchitotenses Associates residents in Los
                    Angeles, who sent funds for humanitarian aid in Suchitoto; this included a scholarship
                    program for university studies that was born in 1999. <br>
                    The scholarship program was growing in number of beneficiaries so in 2004 it was
                    considered to identify a productive project to be executed in Suchitoto whose income
                    would be assigned a percentage for the Suchitoto scholarship program, a debate started
                    on what project would work in Suchitoto, most of them thought of a hotel but a
                    foreigner who collaborated with the scholarship program expressed the idea that
                    Suchitoto offered the conditions for a Spanish school and that was how it was run with
                    this idea in which a former volunteer of the peace corps that worked years ago in El
                    Salvador was key in the formulation of the project and in starting with the idea of ​​the
                    Spanish school.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
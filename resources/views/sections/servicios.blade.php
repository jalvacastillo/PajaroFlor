<section id="courses" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2>Ours Services</h2>
          <p class="col-xs-12">We offers intensive Spanish instruction all year round, with optional short or long-term study.</p>
          <hr class="bottom-line">
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/individual.jpg') }} " class="img-responsive">
            <h2 class="hover">Personalized Classes</h2>
            <figcaption>
              <h3 id="clases-titulo">Personalized Classes</h3>
              <p id="clases-descripcion">We offer one-on-one classes, with a personalized and exclusive Spanish Program for each student according to their level, individual needs and interests of the Spanish language.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2045764782121280&type=1&l=397a7644bc" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="clases" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/grupo.jpg') }} " class="img-responsive">
            <h2 class="hover">Group classes</h2>
            <figcaption>
              <h3 id="group-titulo">Group classes</h3>
              <p id="group-descripcion">We offer group courses when solicited by exclusive groups of students, generally those belonging to an organization, school, or university. <br> Among those is the Japan International Cooperation Agency (JICA) which we’ve given Spanish classes for 10 years.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2063776406986784&type=1&l=95bf4f2bb6" target="_blank"> Fotos <i class="fa fa-link"></i></a>
            </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="group" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/online.jpg') }} " class="img-responsive">
              <h2 class="hover">Online Class</h2>
            <figcaption>
              <h3 id="online-titulo">Online Class</h3>
              <p id="online-descripcion">Online Spanish courses are offered via Skype and contain lessons in grammar, vocabulary, oral and written exercises and conversation practices. Flexible schedules. With an exclusive program for each student according to their level, individual needs and interests of the Spanish language.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2063782806986144&type=1&l=a183761607" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="online" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/homestay.jpeg') }} " class="img-responsive">
              <h2 class="hover">Where to stay</h2>
            <figcaption>
              <h3 id="homestay-titulo">Where to stay</h3>
              <p id="homestay-descripcion">Suchitoto offers you a variety of good quality hostels and hotels; We can offer you excellent hostels that used to be homestays and that today have become well-equipped hostels where our students are always well looked after.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2063783536986071&type=1&l=659d787ec5" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="homestay" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/transporte.jpg') }} " class="img-responsive">
            <h2 class="hover">Transportation</h2>
            <figcaption>
              <h3 id="transporte-titulo">Transportation</h3>
              <p id="transporte-descripcion">If you wish, we will provide transportation from the airport to the school, and once your stay is over, we will take you to the airport.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2063784366985988&type=1&l=50aa767dae" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="transporte" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 padleft-right">
          <figure class="imghvr-fold-up">
            <img src="{{ asset('img/servicios/intercambio.jpg') }} " class="img-responsive">
              <h2 class="hover">Cultural exchange</h2>
            <figcaption>
              <h3 id="cultura-titulo">Cultural exchange</h3>
              <p id="cultura-descripcion">You will have the opportunity to know and enjoy our culture, you will be able to dress our typical costumes and dance native music of our country.
              <br><br><a href="https://www.facebook.com/media/set/?set=a.2063785380319220&type=1&l=eedb39b167" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </figcaption>
          </figure>
          {{-- <a href="javascript:void(0)" name="cultura" class="previsualizar btn btn-bg yellow btn-block">Detail</a> --}}
        </div>
      </div>
    </div>
  </section>


<div class="modal fade" id="modal-info">
  <div class="modal-dialog" style="margin-top: 30px;">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class=" text-center modal-title" id="modal-titulo"></h4>
        </div>
        <div class="modal-body">
          <p id="modal-descripcion">
            
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>

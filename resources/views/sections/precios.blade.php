<section id="pricing" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2>Our Calculator Pricing</h2>
          <hr class="bottom-line">
        </div>

          <div class="row text-center" ng-controller="PresupuestoCtr as this">
            
            <div class="form-group row">
              <h4 for="">What kind of classes do you want?</h4>
              <br>
              <div class="radio-inline" style="padding-left: 0px;">
                <label class="btn btn-info" style="width: 150px;">
                  <input type="radio" ng-model="tipo" value="Presencial" ng-click="this.total = 0" style="display: none;">
                  One-on-one
                </label>
              </div>
              <div class="radio-inline" style="padding-left: 0px;">
                <label class="btn btn-info" style="width: 150px;">
                  <input type="radio" ng-model="tipo" value="Online" ng-click="this.total = 0" style="display: none;">
                  Online
                </label>
              </div>
            </div>
            <div class="col-md-8 col-md-offset-2"  ng-show="tipo == 'Presencial' ">
              
              <div class="form-group col-sm-6">
                <label for="">Number of Days</label>
                <input type="number" min="1" ng-model="dias" class="form-control" placeholder="Days">
              </div>
              <div class="form-group col-sm-6">
                <label for="">Hours per Days</label>
                <select class="form-control" ng-model="horas">
                  <option value="4">4 hours</option>
                  <option value="5">5 hours</option>
                  <option value="6">6 hours</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
                <label for="">Homestay and Food</label>
                <select class="form-control" ng-model="extra">
                  <option value="no">No</option>
                  <option value="1">Homestay</option>
                  <option value="2">Homestay + Food</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
                <label for="">Transportation</label>
                <select class="form-control" ng-model="transporte">
                  <option value="no">No</option>
                  <option value="1">One-way</option>
                  <option value="2">Round-trip</option>
                </select>
              </div>
            </div>

            <div class="col-md-8 col-md-offset-2"  ng-show="tipo == 'Online' ">
              <div class="form-group col-sm-12">
                <label for="">Hours</label>
                <select class="form-control" ng-model="onlinehoras" ng-change="this.getPresupuesto()">
                  <option value="5">5 hours</option>
                  <option value="10">10 hours</option>
                  <option value="15">15 hours</option>
                  <option value="20">20 hours</option>
                  <option value="50">50 hours</option>
                </select>
              </div>
            </div>
            
            <div class="col-xs-12" ng-show="this.total > 0">
              <h1 ng-bind="this.total | currency"></h1>
            </div>
            <div class="col-xs-12" ng-show="tipo == 'Presencial'">
              <br>
              <input type="button" ng-disabled="!this.horas || !this.dias" value="Calculate" ng-click="this.getPresupuesto()" class="btn btn-primary">
            </div>

          </div>

          <div class="row text-center">
            <br><br>
            <p>Or see our <a href="{{ asset('docs/price-table.pdf') }}" target="_blank">price table</a>
            </p>
          </div>

      </div>
    </div>
  </section>

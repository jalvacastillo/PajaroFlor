<section id="work-shop" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2>Program Structure</h2>
          <p class="text-center">
            Our teaching method is a total Spanish immersion. <br>
            We offer Spanish courses throughout the year, 
            which can be for a short or long duration, ranging from one week to 3 or more months.
          <hr class="bottom-line">
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-calendar-check color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Class Days:</h4>
              <p>We offer classes on: <br>
                ✓ weekdays and weekends. <br>
                ✓ We don’t have specific start dates. <br>
                ✓ Classes may begin any day and at any time.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-calendar-alt color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Schedule</h4>
              <p>The most common class schedules are:</p>
              <p>
                8 to 12 pm: 4 hours per day. <br>
                8 to 1 pm: 5 hours per day. <br>
                8 to 12 pm and 2 to 4 pm: 6 hours per day.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-user-clock color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Class Shifts:</h4>
              <p>
                ✓ Only during the morning. <br>
                ✓ Only during afternoons. <br>
                ✓ Mornings and afternoons.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-star color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Start</h4>
              <p>
                We evaluate of their Spanish level for create a proper curriculum based. <br>
                Each student can choose a focus in grammar, conversation or writing for a specific themes or interests.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-map-marked-alt color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Class Durations</h4>
              <p>
                  Minimum: 2 hours per session. <br>
                  Weeks: One week or more.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="service-box text-center">
            <div class="icon-box">
              <i class="fa fa-hands-helping color-green"></i>
            </div>
            <div class="icon-text">
              <h4 class="ser-text">Extra activities</h4>
              <p>
                Our program includes activities such as Typical Foods of El Salvador-Pupusas Workshop, Tour of the city, visit to the cigar craft workshop, Visit to Lake Suchitlán, among others.
                <br><br><a href="https://www.facebook.com/media/set/?set=a.2046834602014298&type=1&l=9069ad2299" target="_blank"> Fotos <i class="fa fa-link"></i></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  
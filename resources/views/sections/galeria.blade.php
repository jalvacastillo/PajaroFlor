<section id="cta-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="text-center">Galery</h2>
        <p class="col-xs-12 text-center">Get to know our city and our school.</p>
        <div class="col-xs-12 col-md-6 text-center">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/bO_eTNs_R5A" allowfullscreen></iframe>
          </div>
        </div>
        <div class="col-xs-12 col-md-6 text-center">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Fu6kb10XaEw" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
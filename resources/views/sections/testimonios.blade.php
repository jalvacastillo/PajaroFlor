<section id="testimonial" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2 class="white">Testimonials</h2>
          <p class="white">
            Know what our students say about us
          </p>
          <hr class="bottom-line bg-white">
        </div>
        @if ($num == 1)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/simon.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Simon Robinson, United Kingdom.</h4>
                <p class="text-par">
                  "I liked the teaching style, the clarity of the notes on the board, the comprehensive understanding of the grammar, the patience of the teacher and the concern for my experience."
                </p>
              </div>
            </div>
          </div>
        </div>
        @endif
        
        @if ($num == 1)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/amy.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Amy Pennington. Universidad Johns Hopkins, United States.</h4>
                <p class="text-par">"Wilian es un profesor excelente! Es obvio que él ama enseñar; la estructura de sus clases, los temas que cobramos en clases y su manera de explicar la gramática fue muy creativa y sofisticada. Su energía y actitud positiva son increíbles."</p>
              </div>
            </div>
          </div>
        </div>
        @endif
        
        @if ($num == 2)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/tifany_peter.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Tiffany y Peter Hall, England.</h4>
                <p class="text-par">"My husband and I spent a week at the Pajaro Flor Spanish School and highly recommend it. Nelson, the manager was very flexible about when we had lessons and he was prepared to open the school on a Sunday as we wanted to start lessons on our first day. Our two teachers Beatriz and Christian were excellent and gave us a good combination of conversation practice and grammar."</p>
              </div>
            </div>
          </div>
        </div>
        @endif
        
        @if ($num == 2)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/ben.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Ben Hale, Ireland.</h4>
                <p class="text-par">"Everything was good. I know that this is very useful and I learned a lot with Cristhian, his family, the school and Suchitoto in general. A big thank you to everyone at the school."</p>
              </div>
            </div>
          </div>
        </div>
        @endif
        
        @if ($num == 3)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/mihela.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Mihela Zupanc, Slovakia.</h4>
                <p class="text-par">"10 days, living with family in Suchitoto and every day of 5 hours private classes with Beatriz. Full immersion. Walked out with enough spanish that I got me quickly almost fluent in it later while travelling Latin America. Thanks Pajaro Flor."</p>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if ($num == 3)
        <div class="col-sm-12 col-md-6">
          <div class="text-comment text-center">
            <div class="media">
              <div class="media-left">
                <a href="#">
                  <img class="media-object" src="{{ asset('img/referencia/chris.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Chriss Swiggum, United States.</h4>
                <p class="text-par">"For me the most important thing in Spanish classes is the relationship between the teacher and the student, in this case everything went well. I do not want the pile of topics, I prefer some themes but deeper and Marta listened to me."</p>
              </div>
            </div>
          </div>
        </div>
        @endif

        <div class="col-xs-12 text-center">
          <br><br>
          <a data-toggle="modal" href='#modal-testimonial' class="btn btn-lg" style="background-color: #5FCF80; border-radius: 25px; color: #ffffff !important; border: 1px solid #5FCF80;">See more</a>
        </div>
        
      </div>

    </div>
  </section>

  <div class="modal fade" id="modal-testimonial">
    <div class="modal-dialog modal-lg" style="margin-top: 30px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Testimonial</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/simon.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Simon Robinson, United Kingdom.</h4>
                    <p class="text-par">
                      "I liked the teaching style, the clarity of the notes on the board, the comprehensive understanding of the grammar, the patience of the teacher and the concern for my experience."
                    </p>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/amy.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Amy Pennington. Universidad Johns Hopkins, United States.</h4>
                    <p class="text-par">"Wilian es un profesor excelente! Es obvio que él ama enseñar; la estructura de sus clases, los temas que cobramos en clases y su manera de explicar la gramática fue muy creativa y sofisticada. Su energía y actitud positiva son increíbles."</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/tifany_peter.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Tiffany y Peter Hall, England.</h4>
                    <p class="text-par">"My husband and I spent a week at the Pajaro Flor Spanish School and highly recommend it. Nelson, the manager was very flexible about when we had lessons and he was prepared to open the school on a Sunday as we wanted to start lessons on our first day. Our two teachers Beatriz and Christian were excellent and gave us a good combination of conversation practice and grammar."</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/ben.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Ben Hale, Ireland.</h4>
                    <p class="text-par">"Everything was good. I know that this is very useful and I learned a lot with Cristhian, his family, the school and Suchitoto in general. A big thank you to everyone at the school."</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/mihela.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Mihela Zupanc, Slovakia.</h4>
                    <p class="text-par">"10 days, living with family in Suchitoto and every day of 5 hours private classes with Beatriz. Full immersion. Walked out with enough spanish that I got me quickly almost fluent in it later while travelling Latin America. Thanks Pajaro Flor."</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="text-comment text-center">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="{{ asset('img/referencia/chris.jpg') }}" alt="Referencia Pajaro Flor" class="img-thumbnail" style="height: 150px;">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Chriss Swiggum, United States.</h4>
                    <p class="text-par">"For me the most important thing in Spanish classes is the relationship between the teacher and the student, in this case everything went well. I do not want the pile of topics, I prefer some themes but deeper and Marta listened to me."</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
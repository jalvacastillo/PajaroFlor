  <section id="cta-2">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="text-center">Do you want to register?</h2>
          <p class="cta-2-txt">Immersion Spanish Course at Pajaro Flor Spanish School Registration</p>
          <div class="cta-2-form text-center">
            {{-- <form action="#" method="post" id="workshop-newsletter-form">
              <input name="" placeholder="Enter Your Email Address" type="email">
              <input class="cta-2-form-submit-btn" value="Subscribe" type="submit">
            </form> --}}
            <a data-toggle="modal" href='#register-modal' class="btn btn-lg" style="background-color: #5FCF80; border-radius: 25px; color: #ffffff !important; border: 1px solid #5FCF80;">Register</a>
          </div>
        </div>
      </div>
    </div>
  </section>

    <div class="modal fade" id="register-modal">
    <div class="modal-dialog" style="margin-top: 30px;">
      <div class="modal-content">
        <form method="POST" action="{{ route('registro') }}">
          {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class=" text-center modal-title">Register!</h4>
          </div>
          <div class="modal-body">
            <label for="nombre">Personal Information:</label>
            <div class="form-group">
              <input type="text" name="nombre"   class="form-control" required placeholder="First Name" autofocus>
              <div class="checkbox-inline" style="margin-top: 10px;"><label><input type="radio" name="titulo" value="Mr." checked> Mr.</label></div>
              <div class="checkbox-inline" style="margin-top: 10px;"><label><input type="radio" name="titulo" value="Mrs."> Mrs.</label></div>
              <div class="checkbox-inline" style="margin-top: 10px;"><label><input type="radio" name="titulo" value="Ms."> Ms.</label></div>
              <input type="text" name="apellido" class="form-control" required placeholder="Last Name" style="margin-top: 10px;">
              <input type="text" name="nacionalidad" class="form-control" required placeholder="Nationality" style="margin-top: 10px;">
              <input type="number" name="edad" class="form-control" required placeholder="Age" style="margin-top: 10px;">
              <input type="text" name="profesion" class="form-control" required placeholder="Profession" style="margin-top: 10px;">
              <input type="email" name="correo" class="form-control" required placeholder="Email" style="margin-top: 10px;">
            </div>
            <div class="form-group">
              <label for="nombre">Spanish Level:</label>
              <select name="nivel" class="form-control">
                <option value="Beginner">Beginner</option>
                <option value="Elementary">Elementary</option>
                <option value="Low intermediate">Low intermediate</option>
                <option value="High intermediate">High intermediate</option>
                <option value="Advanced">Advanced</option>
                <option value="Superior ">Superior </option>
              </select>
            </div>
            <div class="form-group">
              <label for="estudio_espanol">Have you studied Spanish before?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="estudio_espanol" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="estudio_espanol" value="No"> No</label></div>
            </div>
            <div class="form-group">
              <label for="estudio_en">If your answer is yes, where did you study Spanish?:</label>
              <input type="text" name="estudio_en" class="form-control">
            </div>
            <div class="form-group">
              <label for="tiempo">How long did you study Spanish?:</label>
              <input type="text" name="tiempo" class="form-control">
            </div>
            <div class="form-group">
              <label for="horario">Schedule?:</label>
              <select name="horario" class="form-control">
                <option value="8 to 12 pm">8 to 12 pm: 4 hours per day</option>
                <option value="8 to 1 pm">8 to 1 pm: 5 hours per day</option>
                <option value="8 to 12 pm and 2 to 4 pm">8 to 12 pm and 2 to 4 pm: 6 hours per day</option>
              </select>
            </div>
            <div class="form-group">
              <label for="sabado">Classes on Saturday?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="sabado" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="sabado" value="No"> No</label></div>
            </div>
            <div class="form-group">
              <label for="domingo">Classes on Sunday?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="domingo" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="domingo" value="No"> No</label></div>
            </div>
            <div class="form-group">
              <label for="homestay">Do you need accommodation?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="homestay" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="homestay" value="No"> No</label></div>
              <input type="date" class="form-control" name="homestay_entrada" style="margin-top: 10px;">
              <input type="date" class="form-control" name="homestay_salida" style="margin-top: 10px;">
            </div>
            <div class="form-group">
              <label for="food">Do you need food?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="food" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="food" value="No"> No</label></div>
            </div>
            <div class="form-group">
              <label for="transporte">Airport Pickup?:</label>
              <div class="checkbox-inline"><label><input type="radio" name="transporte" value="Si" checked> Si</label></div>
              <div class="checkbox-inline"><label><input type="radio" name="transporte" value="No"> No</label></div>
              <input type="date" class="form-control" name="pickup_entrada" style="margin-top: 10px;">
              <input type="date" class="form-control" name="pickup_salida" style="margin-top: 10px;">
              <input type="text" class="form-control" name="aerolinea" placeholder="Airline" style="margin-top: 10px;">
              <input type="text" class="form-control" name="numero_vuelo" placeholder="Flight Number" style="margin-top: 10px;">
            </div>

            <div class="form-group">
              <label for="interes">Do you have any topic of interest to learn?:</label>
              <input type="text" name="interes" class="form-control" required>
            </div>
            <div class="form-group">
              <label for="dates">Start and End Date:</label>
              <input type="date" class="form-control" name="entrada" required style="margin-top: 10px;">
              <input type="date" class="form-control" name="salida" required style="margin-top: 10px;">
            </div>
            <div class="form-group">
              <label for="nota">Questions or Additional Information?:</label>
              <textarea name="nota" class="form-control" rows="2"></textarea>
            </div>
            <div class="form-group">
              <div class="g-recaptcha" data-sitekey="6LeSqJgUAAAAAKbvyvBKFpYpydDFQKR3OkFZg3Wr" data-callback="recaptchaR_callback"></div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit"class="btn btn-primary" id="recaptchaR"  disabled>Send</button>
          </div>
        </form>
      </div>
    </div>
  </div>
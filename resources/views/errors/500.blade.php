<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pajaro Flor - Spanish School</title>
  <meta name="description" content="Pajaro Flor Community Language Center Spanish School Suchitoto El salvador. Our teachers were trained by theformer Spanish language Director for Peace Corps El Salvador to teachusing internationally recognized methods.">
  <meta name="keywords" content="suchitoto,spanish,school,learn spanish,tours,travel,el salvador">
  <meta name="Reply-to" content="hello@pajaroflor.com">
  <meta name="robots" content="index, follow" >
  <meta name="author" content="Jesus Alvarado">
  <link rel="shortcut icon" type="image/ico" href="{{ asset('/favicon.ico') }}"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  
  <style>
    body {
        background: #dedede;
        font-size: 30px;
    }
    h2{font-size: 2em;}
    p, a{font-size: 1em;}
    .page-wrap {
        min-height: 100vh;
        display: flex;
        flex-direction: row;
        align-items: center;
    }
    .row{
      justify-content: center;
    }
  </style>
</head>

<body>

  
  <div class="page-wrap">
      <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                  <img src="{{ asset('img/logo.png') }}" alt="Logo Pajaro Flor" height="100">
                  <h2>500</h2>
                  <p class="lead">Lo sentimos, hubo un problema vuelve en otro momento.</p>
                  <a href="{{ url('/') }}" class="btn btn-link">Regresar</a>
              </div>
          </div>
      </div>
  </div>


</body>
</html>
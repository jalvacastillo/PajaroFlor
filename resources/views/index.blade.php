@extends('layout')

@section('content')

	@include('header')

	@include('sections.nosotros')
	@include('sections.servicios')
	@include('sections.program')
	@include('sections.testimonios')
	{{-- @include('sections.equipo') --}}
	{{-- @include('sections.galeria') --}}
	@include('sections.precios')
	@include('sections.accion')

	@include('footer')

@endsection
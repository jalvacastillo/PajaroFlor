<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand hidden-sm" href="{{ url('/') }}">Pajaro<span> Flor</span></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#feature">Background</a></li>
        <li><a href="#courses">Courses</a></li>
        <li><a href="#work-shop">Program</a></li>
        <li><a href="#testimonial">Testimonials</a></li>
        <li class="btn-trial"><a href="#footer">Contact us</a></li>
        <li><a style="margin: 10px 0px;" href="https://www.facebook.com/SpanishSchoolPajaroFlor/" target="_blank"><i class="fab fa-facebook-square fa-2x" style="color: #337ab7; margin: 0px;"></i></a></li>
        <li><a style="margin: 10px 0px;" href="https://api.whatsapp.com/send?phone=50371077371" target="_blank"><i class="fab fa-whatsapp-square fa-2x" style="color: #5fcf80; margin: 0px;"></i></a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="banner" style="height: 100vh;">
  <div class="bg-color" style="height: 100vh;">
    <div class="container">
      <div class="row">
        <div class="banner-text text-center">
          <div class="text-border">
            <h1 class="text-dec">Spanish School</h1>
          </div>
          <div class="intro-para text-center quote">
            <p class="big-text">"A Language, a Culture"</p>
            {{-- <p class="big-text">Learn Spanish with us!</p> --}}
            <p class="small-text">Learn Spanish with us!</p>
            <a href="#footer" class="btn get-quote">Contact us</a>
          </div>
          <a href="#feature" class="mouse-hover">
            <div class="mouse"></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
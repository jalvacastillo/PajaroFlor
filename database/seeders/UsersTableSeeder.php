<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Alumno;
use App\Profesor;
use App\Clase;
use App\ClaseAlumno;
use App\ClaseProfesor;
use App\PagoAlumno;
use App\PagoProfesor;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

            $user = new User;
            $user->name = 'Usuario Admin';
            $user->email = 'admin@admin.com';
            // $user->email = 'gnelsonsuchi@gmail.com';
            // $user->password = Hash::make('gp4j4r0');
            $user->password = Hash::make('admin');
            $user->tipo = 'Administrador';
            $user->save();

        for ($i=1; $i <= 10; $i++) { 
            $table = new Alumno;
            $table->nombre = $faker->name;
            $table->nacionalidad = $faker->country;
            $table->sexo = 'Mujer';
            $table->email = $faker->email;
            $table->edad = $faker->numberBetween(18,55);
            $table->nivel = 'Basico';
            $table->save();
        }

        for ($i=1; $i <= 10; $i++) { 
            $table = new Profesor;
            $table->nombre = $faker->name;
            $table->save();
        }

        for ($i=1; $i <= 10; $i++) { 
            $table = new PagoAlumno;
            $table->alumno_id = $i;
            $table->horas = $faker->numberBetween(1,6);
            $table->total = $faker->numberBetween(10,25);
            $table->save();
        }

        for ($i=1; $i <= 10; $i++) { 
            $table = new PagoProfesor;
            $table->profesor_id = $i;
            $table->alumno_id = $i;
            $table->horas = $faker->numberBetween(1,6);
            $table->total = $faker->numberBetween(10,25);
            $table->save();
        }

        for ($i=1; $i <= 10; $i++) { 
            $table = new Clase;
            $table->inicio  = $faker->date;
            $table->final     = $faker->date;
            $table->horas   = $faker->numberBetween(10,25);
            $table->estado   = 'En Proceso';
            $table->save();

            $table = new ClaseAlumno;
            $table->clase_id   = $i;
            $table->alumno_id   = $i;
            $table->save();

            $table = new ClaseProfesor;
            $table->clase_id   = $i;
            $table->profesor_id   = $i;
            $table->horas   = $faker->numberBetween(5,15);
            $table->save();
        }
            
    }
}

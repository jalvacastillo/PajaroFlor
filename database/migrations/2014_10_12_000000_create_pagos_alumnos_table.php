<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosAlumnosTable extends Migration {

    public function up()
    {
        Schema::create('pagos_alumnos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('alumno_id');
            $table->decimal('horas');
            $table->decimal('total');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('pagos_alumnos');
    }

}

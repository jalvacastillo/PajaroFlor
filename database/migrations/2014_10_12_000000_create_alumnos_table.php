<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration {

    public function up()
    {
        Schema::create('alumnos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre',100)->nullable();
            $table->string('nacionalidad',100)->nullable();
            $table->string('sexo',50)->nullable();
            $table->string('email',50)->nullable();
            $table->string('telefono',50)->nullable();
            $table->string('edad')->nullable();
            $table->string('nivel')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('alumnos');
    }

}

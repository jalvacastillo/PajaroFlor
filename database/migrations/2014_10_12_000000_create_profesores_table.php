<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresTable extends Migration {

    public function up()
    {
        Schema::create('profesores', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre',100)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('profesores');
    }

}

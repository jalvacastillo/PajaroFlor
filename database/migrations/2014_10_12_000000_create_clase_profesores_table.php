<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaseProfesoresTable extends Migration {

    public function up()
    {
        Schema::create('clase_profesores', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('clase_id');
            $table->decimal('horas')->nullable();
            $table->integer('profesor_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clase_profesores');
    }

}

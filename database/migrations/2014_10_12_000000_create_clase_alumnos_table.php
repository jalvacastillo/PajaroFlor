<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaseAlumnosTable extends Migration {

    public function up()
    {
        Schema::create('clase_alumnos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('clase_id');
            $table->integer('alumno_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clase_alumnos');
    }

}

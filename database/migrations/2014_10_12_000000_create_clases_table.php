<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasesTable extends Migration {

    public function up()
    {
        Schema::create('clases', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('inicio');
            $table->date('final');
            $table->decimal('horas')->nullable();
            $table->string('estado');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clases');
    }

}

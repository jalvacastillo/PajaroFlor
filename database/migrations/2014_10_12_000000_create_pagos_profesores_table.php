<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosProfesoresTable extends Migration {

    public function up()
    {
        Schema::create('pagos_profesores', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('profesor_id');
            $table->integer('alumno_id');
            $table->decimal('horas');
            $table->decimal('total');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('pagos_profesores');
    }

}

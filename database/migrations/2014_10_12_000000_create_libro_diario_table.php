<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibroDiarioTable extends Migration {

    public function up()
    {
        Schema::create('libro_diario', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('concepto');
            $table->string('tipo');
            $table->decimal('total');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('libro_diario');
    }

}

<?php

    use App\Http\Controllers\Api\TestimoniosController;

    Route::get('/testimonios',                    [TestimoniosController::class, 'index']);
    Route::get('/testimonio/{id}',                [TestimoniosController::class, 'read']);
    Route::get('/testimonios/buscar/{text}',      [TestimoniosController::class, 'search']);
    Route::post('/testimonios/filtrar',           [TestimoniosController::class, 'filter']);
    Route::post('/testimonio',                    [TestimoniosController::class, 'store']);
    Route::delete('/testimonio/{id}',             [TestimoniosController::class, 'delete']);

?>

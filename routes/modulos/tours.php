<?php

    use App\Http\Controllers\Api\Tours\ToursController;

    Route::get('/tours',                    [ToursController::class, 'index']);
    Route::get('/tour/{id}',                [ToursController::class, 'read']);
    Route::get('/tours/buscar/{text}',      [ToursController::class, 'search']);
    Route::post('/tours/filtrar',           [ToursController::class, 'filter']);
    Route::post('/tour',                    [ToursController::class, 'store']);
    Route::delete('/tour/{id}',             [ToursController::class, 'delete']);

?>

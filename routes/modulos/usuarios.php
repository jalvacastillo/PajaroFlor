<?php

    use App\Http\Controllers\Api\UsuariosController;

    Route::get('/usuarios',                    [UsuariosController::class, 'index']);
    Route::get('/usuario/{id}',                [UsuariosController::class, 'read']);
    Route::get('/usuarios/buscar/{text}',      [UsuariosController::class, 'search']);
    Route::post('/usuarios/filtrar',           [UsuariosController::class, 'filter']);
    Route::post('/usuario',                    [UsuariosController::class, 'store']);
    Route::delete('/usuario/{id}',             [UsuariosController::class, 'delete']);

?>

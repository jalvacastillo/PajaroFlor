<?php

    use App\Http\Controllers\Api\Habitaciones\HabitacionesController;
    use App\Http\Controllers\Api\Habitaciones\ImagenesController;

    Route::get('/habitaciones',                    [HabitacionesController::class, 'index']);
    Route::get('/habitacion/{id}',                [HabitacionesController::class, 'read']);
    Route::get('/habitaciones/buscar/{text}',      [HabitacionesController::class, 'search']);
    Route::post('/habitaciones/filtrar',           [HabitacionesController::class, 'filter']);
    Route::post('/habitacion',                    [HabitacionesController::class, 'store']);
    Route::delete('/habitacion/{id}',             [HabitacionesController::class, 'delete']);

    Route::get('/habitacion/{id}/imagenes',                    [ImagenesController::class, 'index']);
    Route::get('/habitacion/imagen/{id}',                [ImagenesController::class, 'read']);
    Route::post('/habitacion/imagen',                    [ImagenesController::class, 'store']);
    Route::delete('/habitacion/imagen/{id}',             [ImagenesController::class, 'delete']);

?>

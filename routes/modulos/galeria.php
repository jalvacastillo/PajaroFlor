<?php

    use App\Http\Controllers\Api\GaleriaController;

    Route::get('/galerias',                    [GaleriaController::class, 'index']);
    Route::get('/galeria/{id}',                [GaleriaController::class, 'read']);
    Route::get('/galerias/buscar/{text}',      [GaleriaController::class, 'search']);
    Route::post('/galerias/filtrar',           [GaleriaController::class, 'filter']);
    Route::post('/galeria',                    [GaleriaController::class, 'store']);
    Route::delete('/galeria/{id}',             [GaleriaController::class, 'delete']);

?>

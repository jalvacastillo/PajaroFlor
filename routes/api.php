<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/prueba', function(){ return Response()->json(['message' => 'Success'], 200); });

require base_path('routes/modulos/auth.php');
		
Route::group(['middleware' => ['jwt.auth']], function () {


		require base_path('routes/modulos/dash.php');
		require base_path('routes/modulos/facturacion.php');
		require base_path('routes/modulos/habitaciones.php');
		require base_path('routes/modulos/testimonios.php');
		require base_path('routes/modulos/tours.php');
		require base_path('routes/modulos/galeria.php');
		require base_path('routes/modulos/usuarios.php');

		
});


Route::get('/prueba/factura', function () { 
	return view('reportes/pruebas/factura');	
});

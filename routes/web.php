<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashController;
use App\Http\Controllers\AlumnosController;
use App\Http\Controllers\ClasesController;
use App\Http\Controllers\PagosAlumnosController;
use App\Http\Controllers\PagosProfesoresController;
use App\Http\Controllers\LibroDiarioController;
use App\Http\Controllers\ProfesoresController;


Route::get('/', function () {
	$num = null;
	$num = rand(1,3);

	return view('index', compact('num')); 
});

Route::post('correo', [HomeController::class, 'correo'])->name('correo');
Route::post('registro', [HomeController::class, 'registro'])->name('registro');

Auth::routes();


Route::prefix('admin')->group(function () {
    Route::get('/', [DashController::class, 'index'])->name('dash');

    Route::get('/alumnos',          [AlumnosController::class, 'index'])->name('alumnos');
    Route::get('/alumno/{id?}',     [AlumnosController::class, 'read'])->name('alumno');
    Route::post('/alumno/{id?}',    [AlumnosController::class, 'store']);
    Route::get('/alumno/clases/{id?}',    [AlumnosController::class, 'clases'])->name('alumnoClases');
    Route::get('/alumno/pagos/{id?}',    [AlumnosController::class, 'pagos'])->name('alumnoPagos');
    Route::get('/alumno-delete/{id?}',     [AlumnosController::class, 'deleteAlumno'])->name('deleteAlumno');

    Route::get('/clases',           [ClasesController::class, 'index'])->name('clases');
    Route::get('/clase/{id?}',      [ClasesController::class, 'read'])->name('clase');
    Route::post('/clase/{id?}',     [ClasesController::class, 'store']);
    Route::get('/clase-delete/{id?}',     [ClasesController::class, 'deleteClase'])->name('deleteClase');


    Route::post('/clase-add-alumno',     [ClasesController::class, 'addAlumno'])->name('addAlumnoToClase');
    Route::get('/clase-delete-alumno/{id}',     [ClasesController::class, 'deleteAlumno'])->name('deleteAlumnoToClase');
    Route::post('/clase-add-profesor',     [ClasesController::class, 'addProfesor'])->name('addProfesorToClase');
    Route::get('/clase-delete-profesor/{id}',     [ClasesController::class, 'deleteProfesor'])->name('deleteProfesorToClase');

    Route::get('/pagos-alumnos',           [PagosAlumnosController::class, 'index'])->name('pagosAlumnos');
    Route::get('/pago-alumno/{id?}',      [PagosAlumnosController::class, 'read'])->name('pagoAlumno');
    Route::post('/pago-alumno/{id?}',     [PagosAlumnosController::class, 'store']);
    Route::get('/pago-alumno-delete/{id?}',     [PagosAlumnosController::class, 'deletePago'])->name('deletePagoAlumno');

    Route::get('/pagos-profesores',           [PagosProfesoresController::class, 'index'])->name('pagosProfesores');
    Route::get('/pago-profesor/{id?}',      [PagosProfesoresController::class, 'read'])->name('pagoProfesor');
    Route::post('/pago-profesor/{id?}',     [PagosProfesoresController::class, 'store']);
    Route::get('/pago-profesor-delete/{id?}',     [PagosProfesoresController::class, 'deletePago'])->name('deletePagoProfesor');

    Route::get('/libro-diario',                 [LibroDiarioController::class, 'index'])->name('libro');
    Route::get('/libro-diario/registro/{id?}',  [LibroDiarioController::class, 'read'])->name('libroRegistro');
    Route::post('/libro-diario/registro/{id?}',     [LibroDiarioController::class, 'store']);
    Route::get('/libro-diario-delete-registro/{id?}',     [LibroDiarioController::class, 'deleteRegistro'])->name('deleteRegistro');

    Route::get('/profesores',       [ProfesoresController::class, 'index'])->name('profesores');
    Route::get('/profesor/{id?}',   [ProfesoresController::class, 'read'])->name('profesor');
    Route::post('/profesor/{id?}',  [ProfesoresController::class, 'store']);
    Route::get('/profesor/clases/{id?}',    [ProfesoresController::class, 'clases'])->name('profesorClases');
    Route::get('/profesor/pagos/{id?}',    [ProfesoresController::class, 'pagos'])->name('profesorPagos');
    Route::get('/profesor-delete/{id?}',     [ProfesoresController::class, 'deleteProfesor'])->name('deleteProfesor');

});
